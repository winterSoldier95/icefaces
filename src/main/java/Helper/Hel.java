package Helper;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import Som.Som.Base;

public class Hel extends Base{
	
	
	
	public static void HighLightElement(WebElement ele){
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("arguments[0].setAttribute('style', 'background: turquoise; border: 2px solid red;');", ele);
		try 
		{
			Thread.sleep(1000);
		}
		catch (InterruptedException e) {	    
			System.out.println(e.getMessage());
		} 	    
		js.executeScript("arguments[0].setAttribute('style','border: solid 2px white');", ele); 
	}
	
	
	public static void SendKeysText(String elementOR,String text){	
		WebElement ele =driver.findElement(By.xpath(OR.getProperty(elementOR)));
		HighLightElement(ele);
		ele.sendKeys(text);
	}
	
	public static void Click(String elementOR){
		WebElement ele =driver.findElement(By.xpath(OR.getProperty(elementOR)));
		HighLightElement(ele);
		ele.click();		
	}
	
	
	}
	
	

