package Som.Som;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Base {
	
	public static WebDriver driver = null;
	public static Properties CONFIG = null;
	public static Properties OR = null;
	
	public void loadProperties() throws Exception{
		FileInputStream fis = new FileInputStream(new File(System.getProperty("user.dir")+"\\Properties\\CONFIG.properties"));
		CONFIG = new Properties();
		CONFIG.load(fis);
		fis.close();	
		
		FileInputStream fis1 = new FileInputStream(new File(System.getProperty("user.dir")+"\\Properties\\OR.properties"));
		OR = new Properties();
		OR.load(fis1);
		fis1.close();
		
		
	}
	
	public static WebDriver getBrowser(String browser){
		
		if(browser.equalsIgnoreCase("Chrome")){
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");

			ChromeOptions options = new ChromeOptions(); 
			options.addArguments("disable-infobars");
			options.addArguments("--start-maximized");
			options.addArguments("--ignore-certificate-errors");
			options.addArguments("--disable-popup-blocking");
			options.addArguments("--incognito");
			options.addArguments("disable-notifications");
			driver = new ChromeDriver(options);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		}
		return driver;
	}

}
