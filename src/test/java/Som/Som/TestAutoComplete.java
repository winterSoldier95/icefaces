package Som.Som;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;



public class TestAutoComplete extends Base{

	
	WebDriver driver ;
	AutoComplete ac = null;
	
	
	@BeforeMethod
	public void beforeMethod() throws Exception{
		loadProperties();
		//
		driver = Base.getBrowser(CONFIG.getProperty("browser"));
		driver.get(CONFIG.getProperty("url"));
	}

	@Test
	public void TestAutoCompleteMtd(){
		ac = PageFactory.initElements(driver, AutoComplete.class );
		Assert.assertTrue(ac.Check1());
	}
}
